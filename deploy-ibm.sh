#!/bin/bash

## Config
CF_APP=$1
USERNAME=$2
PASSWORD=$3
IMAGE=tomanwalker/couchdb-loadtest

if [[ $# -ne 3 ]]; then
	echo "Illegal number of parameters = " $# 
	echo "Usage: ${0} <app> <user> <pass> "
	exit 2
fi

## Run
echo "CF_APP=$CF_APP"

# exit when any command fails
set -e

cf push "${CF_APP}" -n ${CF_APP} -i 1 -m 256M --docker-image ${IMAGE} --no-start

cf set-env ${CF_APP} USERNAME ${USERNAME}
cf set-env ${CF_APP} PASSWORD ${PASSWORD}

cf start ${CF_APP}
