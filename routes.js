/*
 * Routes definition
 * 
 */

// ### Dependencies
var path = require('path');
var express = require('express');
var log = require('loglevel');
var config = require('./config.js');
var fs = require('fs');
var LeanCache = require('lean-cache');

var authHelper = require('./server/auth.js');
var loadHelper = require('./server/loadRequest.js');

// ### Config
var router = express.Router();
router.use(express.json()); // body-praser is part of the express since 4.16.0
log.setLevel(config.LOG_LEVEL);

var cacheLoadFunc = function(id, callback){
	return callback('Not found = ' + id, null);
};
var reportCache = new LeanCache({
	size: 10,
	load: cacheLoadFunc
});

// ### Func
var base = function(req, res, next){
	log.info("base :: %s = %s", req.method, encodeURI(req.originalUrl));
	res.set('Cache-Control', 'no-store, no-cache, must-revalidate, private');
	return next();
};

var indexPage = function(req, res){
	// redirect to ui
	return res.redirect('/home');
};

var mainPage = function(req, res){
	return res.sendFile(path.join(__dirname, '/client/index.html'));
};

var inputValidation = function(req, res, next){
	
	if( !req.body.req ){
		return res.status(400).json({err: "<req> missing"});
	}
	if( !req.body.con ){
		return res.status(400).json({err: "<con> missing"});
	}
	if( !req.body.db ){
		return res.status(400).json({err: "<db> missing"});
	}
	if( !req.body.url ){
		return res.status(400).json({err: "url missing"});
	}
	
	return next();
	
};

var runTest = function(req, res){
	
	log.debug("runTest - body = %s", JSON.stringify(req.body));
	
	var date = new Date();
	dateStr = date.toISOString();
	
	loadHelper.runBasicScenario(dateStr, req.body, reportCache, function(err, body){
		
		if(err){
			log.error("runTest - error - id = %s | err = %j", dateStr, err);
			return res.status(500).json({id: dateStr, err: err});
		}
		
		log.info("runTest - accepted - id = %s", dateStr);
		return res.status(202).json({id: dateStr, ok:true });
	});
	
};

var checkStatus = function(req, res){
	
	log.debug("checkStatus - id = %s", req.params.id);
	
	reportCache.get(req.params.id, function(err, value){
		
		if(err){
			log.debug("checkStatus - cache.err = %j", err);
			return res.status(404).json(err);
		}
		
		log.debug("checkStatus - cache = %j", value);
		return res.status(200).json(value);
	});
	
};

// ### Routes
router.use('/', base);

if( config.USERNAME && config.PASSWORD ){
	var auth = authHelper.basicAuth(config.USERNAME, config.PASSWORD);
	router.use('/', auth);
}
else{
	log.error('Warning! USERNAME or PASSWORD not set!');
}

router.get('/', indexPage);
router.get('/home', mainPage);

router.post('/run', inputValidation, runTest);
router.get('/status/:id', checkStatus);


// ### Export
module.exports = router;


