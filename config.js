
// ### Export
module.exports = {
	port: Number(process.env.PORT || 8000),
	ip: process.env.IP || '0.0.0.0',
	LOG_LEVEL: process.env.LOG_LEVEL || "DEBUG",
	USERNAME: process.env.USERNAME,
	PASSWORD: process.env.PASSWORD
};


