FROM node:lts-alpine

## install

WORKDIR /base
COPY . .
RUN npm install

## configure
ENV PORT 8080

EXPOSE ${PORT}

CMD ["node", "app.js"]


