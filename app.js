/*
 * Apache Bench Web interface wrapper
 *
 * 2019 - [Alex Shkunov] Initial implementation (v1)
 * 
 */

// ### Dependencies
var express = require('express');
var log = require('loglevel');
var config = require('./config.js');

log.setLevel(config.LOG_LEVEL);

// ### Process
process.on('SIGINT', function() {
	process.exit();
});

// ### Set up Routing
var server = express();
var routes = require('./routes.js');
server.use(routes);

// ### Server start
server.listen(config.port, config.ip, function () {
	
	log.info("Server starting on %s", config.port);
	
});

// ### Export
module.exports = server;


