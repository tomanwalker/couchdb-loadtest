
// ### Dependencies
var log = require('loglevel');
log.setLevel("DEBUG");

// ## Export
var ns = {};
module.exports = ns;

// ## Func
ns.basicAuth = function(argUser, argPassword){
	return function(req, res, next){
		if (!req.headers.authorization) {
			log.info("ERR! Auth header missing - wsra = %s | agent = %s | path = %s", 
				req.headers["$wsra"], req.headers["user-agent"], req.path);
			
			res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
			return next({status: 401, message: "Unauthorized"});
		}
		
		var auth = new Buffer(req.headers.authorization.substring(6), 'base64').toString().split(':');
		var userMatch = (auth[0] === argUser);
		var passMatch = (auth[1] === argPassword);
		
		if (!userMatch || !passMatch){
			log.error("ERR! Auth credentials are wrong - user = %s | userMatch = %s | passMatch = %s | ip = %s | path = %s", 
				auth[0], userMatch, passMatch, req.headers["$wsra"], req.path);
			
			res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
			return next({status: 401, message: "Unauthorized"});
		}
		
		res.locals.basicAuthPass = true;
		return next();
	};
};


