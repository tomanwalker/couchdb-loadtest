
// ### Dependencies
var log = require('loglevel');
var config = require('./../config.js');
var loadtest = require('loadtest');

// ## Config
log.setLevel(config.LOG_LEVEL);

// ## Export
var ns = {};
module.exports = ns;

// ## Func
ns.getGeneratorCreate = function(db, docListString){
	
	var counter = 0;
	log.debug('getGeneratorCreate - start - c = %s', counter);
	
	return function(params, options, client, callback) {
		
		log.debug('getGeneratorCreate - mid - c = %s | body = %s', counter, docListString[ counter ]);
		
		options.path = '/' + db;
		options.body = docListString[ counter++ ];
		
		options.headers['Content-Type'] = 'application/json';
		options.headers['Content-Length'] = options.body.length;
		
		var request = client(options, callback);
		request.write(options.body);
		return request;
	};
};
ns.getGeneratorRead = function(db, docs){
	
	var counter = 0;
	
	return function(params, options, client, callback) {
		options.path = '/' + db + '/' + docs[ counter++ ]._id;
		return client(options, callback);
	};
};
ns.getGeneratorUpdate = function(db, docs, docListString){
	
	var counter = 0;
	
	return function(params, options, client, callback) {
		options.path = '/' + db + '/' + docs[ counter ]._id;
		options.body = docListString[ counter++ ];
		options.headers['Content-Type'] = 'application/json';
		options.headers['Content-Length'] = options.body.length;
		
		var request = client(options, callback);
		request.write(options.body);
		return request;
	};
};
ns.getGeneratorDelete = function(db, docs){
	
	var counter = 0;
	
	return function(params, options, client, callback) {
		
		options.path = '/' + db + '/' + docs[ counter ]._id + '?rev=' + docs[ counter++ ]._rev;
		return client(options, callback);
	};
};

ns.checkDatabase = function(nano, db){
	
	log.debug('checkDatabase - start = %s', db);
	
	return new Promise(function(resolve, reject){
		nano.db.get(db, function(err, body) {
			if (err) {
				log.debug('checkDatabase - get.err = %s', err.message );
				
				return nano.db.create(db, function(errC, bodyC){
					if(errC){
						log.debug('checkDatabase - create.err = %s', errC.message );
						return reject(errC);
					}
					
					log.debug('checkDatabase - create.body = %s', JSON.stringify(bodyC) );
					return resolve('created');
				});
			}
			
			log.debug('checkDatabase - get.body = %s', JSON.stringify(body) );
			return resolve('exist');
		});
	});
};

ns.runLoad = function(options){
	log.info("runLoad - start...");
	
	return new Promise(function(resolve, reject){
		loadtest.loadTest(options, function(error, result) {
			if (error){
				log.error('runLoad - error = %s', error);
				return reject(error);
			}
			
			log.info('runLoad - complete - result = %s', JSON.stringify(result) );
			return resolve(result);
		});
	});
};

ns.satusCallback = function(req, docs){
	return function(error, result, latency){
		if( req <= 5){
			log.debug('statusCallback - result = %j', result);
			log.debug('statusCallback - err = %j', error);
			log.debug('statusCallback - request = %s | instance = %s ', result.requestIndex, result.instanceIndex);
		}
		
		if( docs ){
			
			var parsedBody = null;
			
			try{
				parsedBody = JSON.parse( result.body );
			}
			catch(e){
				log.error('statusCallback - body.json = %s', result.body);
			}
			
			if( parsedBody && parsedBody.rev ){
				var targetDoc = docs.find( d => d._id === parsedBody.id );
				targetDoc._rev = parsedBody.rev;
			}
			else{
				log.debug('statusCallback - no REV = %j', result.body);
			}
		}
	};
};

ns.saveResult = function(nano, db, result){
	nano.use(db).insert(result, function(err, body){
		log.debug('saveResult - callback - err = %j | body = %j', err, body);
	});
};

ns.runBasicScenario = async function(id, opts, cache, callback){
	
	log.debug('runBasicScenario - start - id = %s | opts = %s', id, JSON.stringify(opts) );
	
	// -- INIT
	var reqCreate = opts.req;
	var reqRead = reqCreate;
	var reqUpdate = reqRead;
	var reqDelete = reqRead;
	
	var docs = [];
	var docListString = [];
	var result = {
		_id: ("a-result-" + id), e:null, t: null,
		c: null, r: null, u: null, d: null
	};
	cache.set(id, result);
	
	var nano = require('nano')(opts.url);
	
	// Create Database
	try {
		await ns.checkDatabase(nano, opts.db);
		callback(null, 'ok');
	}
	catch(e){
		log.debug('runBasicScenario - db.await - catch = %s', JSON.stringify(e) );
		return callback(e, null);
	}
	
	// Generate docs
	// Need to generate additional, just in case of
	// https://github.com/alexfernandez/loadtest/issues/130
	var generateAmount = reqCreate + 20;
	for(var i=0; i < generateAmount; i++){
		var docId = "z-" + id + "-" + i;
		var doc = { _id: docId, group: id, name: "abc", num: 84593, list:["a", "b", "c"] };
		docs.push( doc );
		docListString.push( JSON.stringify(doc) );
	}
	
	log.debug('runBasicScenario - docs - d.len = %s | s.len = %s', docs.length, docListString.length);
	
	// -- CREATE
	log.info('---- CREATE = %s', id);
	
	var optionsCreate = {
		url: opts.url,
		maxRequests: reqCreate,
		concurrency: opts.con,
		method: 'POST',
		requestGenerator: ns.getGeneratorCreate(opts.db, docListString),
		statusCallback: ns.satusCallback(reqCreate, docs)
	};
	
	try {
		result.c = await ns.runLoad( optionsCreate );
		docListString = docs.map( d => JSON.stringify(d) ); // statusCallback have set "1" revs
	}
	catch(e){
		log.debug('runBasicScenario - create.await - catch = %s', JSON.stringify(e) );
		result.e = e;
		return ns.saveResult(nano, opts.db, result);
	}
	
	// -- READ
	log.info('---- READ = %s', id);
	
	var optionsRead = {
		url: opts.url,
		maxRequests: reqRead,
		concurrency: opts.con,
		method: 'GET',
		requestGenerator: ns.getGeneratorRead(opts.db, docs)
	};
	
	try {
		result.r = await ns.runLoad( optionsRead );
	}
	catch(e){
		log.debug('runBasicScenario - read.await - catch = %s', JSON.stringify(e) );
		result.e = e;
		return ns.saveResult(nano, opts.db, result);
	}
	
	// -- UPDATE
	log.info('---- UPDATE = %s', id);
	
	var optionsUpdate = {
		url: opts.url,
		maxRequests: reqUpdate,
		concurrency: opts.con,
		method: 'PUT',
		requestGenerator: ns.getGeneratorUpdate(opts.db, docs, docListString),
		statusCallback: ns.satusCallback(reqUpdate, docs)
	};
	
	if( docs.length < 10 ){
		log.debug('runBasicScenario - before.update - docs = %j', 
			docs.map( function(d){ return {_id: d._id, _rev: d._rev}; }) 
		);
	}
	
	try {
		result.u = await ns.runLoad( optionsUpdate );
	}
	catch(e){
		log.debug('runBasicScenario - update.await - catch = %s', JSON.stringify(e) );
		result.e = e;
		return ns.saveResult(nano, opts.db, result);
	}
	
	// -- DELETE
	log.info('---- DELETE = %s', id);
	
	var optionsDelete = {
		url: opts.url,
		maxRequests: reqDelete,
		concurrency: opts.con,
		method: 'DELETE',
		requestGenerator: ns.getGeneratorDelete(opts.db, docs)
	};
	
	try {
		result.d = await ns.runLoad( optionsDelete );
	}
	catch(e){
		log.debug('runBasicScenario - read.await - catch = %s', JSON.stringify(e) );
		result.e = e;
		return ns.saveResult(nano, opts.db, result);
	}
	
	// -- SAVE Final results
	var collected = 0;
	var sumReq = 0;
	var sumErr = 0;
	var sumTime = 0;
	var sumMin = 0;
	var sumAvg = 0;
	var sumMax = 0;
	
	for(var p in result){
		if( result[p] && result[p].rps ){
			sumReq += result[p].rps;
			sumErr += result[p].totalErrors;
			log.debug('INIT sec = %s | Round = %s', result[p].totalTimeSeconds, Math.round(result[p].totalTimeSeconds, 2));
			sumTime += Math.round(result[p].totalTimeSeconds, 2);
			
			sumMin += result[p].minLatencyMs;
			sumAvg += result[p].meanLatencyMs;
			sumMax += result[p].maxLatencyMs;
			
			collected++;
		}
	}
	
	result.t = {
		totalTimeSeconds: sumTime,
		rps: (sumReq / collected),
		min: (sumMin / collected),
		avg: (sumAvg / collected),
		max: (sumMax / collected),
		errors: sumErr
	};
	
	log.info('runBasicScenario - RESULT = %j', result);
	return ns.saveResult(nano, opts.db, result);
	
};


